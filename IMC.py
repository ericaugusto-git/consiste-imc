import csv
import re

def main():
    with open("dataset.csv", "r") as data:
        reader = csv.DictReader(data, delimiter=';')
        for row in reader:
            #checando campos vazios
            if '' not in (row["Peso (kg)"], row["Altura (m)"]):
                #substituindo a ',' pelo '.' e então calculando o IMC
                imc = float(row["Peso (kg)"].replace(',','.')) / pow(float(row["Altura (m)"].replace(',','.')), 2)
                #regex substitui espaços por um único espaço i.e ferreira      da silva = ferreira da silva
                nome_completo = row['Primeiro Nome'].strip() + " " + re.sub("\s+", " ", row['Sobrenomes']).strip()
                resultado = (f'{nome_completo.upper()} {imc:.2f}'.replace('.',','))
                print(resultado)
                with open("[meuNomeCompleto].txt", "a") as file_resultado:
                    file_resultado.write(resultado + "\n")


main()